# README #

## Setup

**Requirements**
- docker & docker-compose

Run the following commands:
```
docker-compose up -d
docker-compose exec php-fpm composer install
docker-compose exec php-fpm bin/console doctrine:migrations:migrate
docker-compose exec php-fpm bin/console doctrine:fixture:load
docker-compose exec php-fpm bin/phpunit

```

**To run the test you must execute :
```
docker-compose exec php-fpm bin/phpunit
```
### Access to the Web
www.localhost:8080

The page has 3 parts
#### Part 1
This is the vending machine.

You can insert coins pressing the different buttons [In 0.05]..[In 1.00]

To Recover the coins inserted you can press de [Return] button.

In order to obtain some product you must press some button labeled [Select]

#### Part 2
Status CashBox

This section shows the coins that the machine has.

#### Part 3
Maintenance section

In this section you can set the items you want in the machine.