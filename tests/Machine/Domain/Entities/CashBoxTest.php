<?php

namespace App\Tests\Machine\Domain\Entities;

use App\Machine\Domain\Entities\CashBox;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class CashBoxTest extends TestCase
{
    /** @test */
    public function whenInstantiateAEntityTheValuesAreCorrect()
    {
        $id = Uuid::uuid4();
        $entity = new CashBox();
        $entity->setId($id);
        $entity->setCoin005(10);
        $entity->setCoin010(10);
        $entity->setCoin025(10);
        $entity->setCoin100(10);

        $this->assertEquals(10, $entity->getCoin005());
        $this->assertEquals(10, $entity->getCoin010());
        $this->assertEquals(10, $entity->getCoin025());
        $this->assertEquals(10, $entity->getCoin100());
    }

    /** @test */
    public function whenInstanciateAnEntityWithIdWrongItThrowAnException()
    {
        $this->expectExceptionMessage('Argument');
        $entity = new CashBox();
        $entity->setId(234);
    }
}
