<?php

namespace App\Tests\Machine\Domain\Entities;

use App\Machine\Domain\Entities\CashBox;
use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Entities\Machine;
use App\Machine\Domain\Entities\Wallet;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ItemTest extends TestCase
{
    const MACHINE_ID = 'dcb8ed16-21d4-4f63-9db7-24b74863b0f8';
    const ITEM_ID = '67f7dac7-88f7-4f92-bda5-471be8619232';
    const CASHBOX_ID = '17891ab6-1755-443b-984d-da60a7e43fd8';
    const WALLET_ID = 'f5b0429b-f39d-4c5c-9041-84ff2b1bde9b';

    /** @test */
    public function whenInstantiateAEntityTheValuesAreCorrect()
    {
        $machine = $this->buildMachine();
        $entity = new Item();
        $entity->setId(Uuid::fromString(self::ITEM_ID));
        $entity->setStock(10);
        $entity->setName('NAME');
        $entity->setPosition(1);
        $entity->setPrice(100);
        $entity->setMachine($machine);

        $this->assertEquals($machine, $entity->getMachine());
        $this->assertEquals(self::ITEM_ID, $entity->getId());
        $this->assertEquals(10, $entity->getStock());
        $this->assertEquals('NAME', $entity->getName());
        $this->assertEquals(1, $entity->getPosition());
        $this->assertEquals(100, $entity->getPrice());

    }

    /** @test */
    public function whenInstanciateAnEntityWithIdWrongItThrowAnException()
    {
        $this->expectExceptionMessage('Argument');
        $entity = new Item();
        $entity->setId(234);
    }

    private function buildMachine(): Machine
    {
        $cashBox = new CashBox();
        $cashBox->setId(Uuid::fromString(self::CASHBOX_ID));
        $cashBox->setCoin005(5);
        $cashBox->setCoin010(10);
        $cashBox->setCoin025(25);
        $cashBox->setCoin100(100);

        $wallet = new Wallet();
        $wallet->setId(Uuid::fromString(self::WALLET_ID));
        $wallet->setCoin005(5);
        $wallet->setCoin010(10);
        $wallet->setCoin025(25);
        $wallet->setCoin100(100);

        $machine = new Machine();
        $machine->setId(Uuid::fromString(self::MACHINE_ID));
        $machine->setStatus(10);
        $machine->setCashBox($cashBox);
        $machine->setWallet($wallet);

        return $machine;
    }
}
