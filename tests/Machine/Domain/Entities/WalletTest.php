<?php

namespace App\Tests\Machine\Domain\Entities;

use App\Machine\Domain\Entities\Wallet;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class WalletTest extends TestCase
{
    /** @test */
    public function whenInstantiateAEntityTheValuesAreCorrect()
    {
        $id = Uuid::uuid4();
        $entity = new Wallet();
        $entity->setId($id);
        $entity->setCoin005(10);
        $entity->setCoin010(10);
        $entity->setCoin025(10);
        $entity->setCoin100(10);

        $this->assertEquals(10, $entity->getCoin005());
        $this->assertEquals(10, $entity->getCoin010());
        $this->assertEquals(10, $entity->getCoin025());
        $this->assertEquals(10, $entity->getCoin100());
    }

    /** @test */
    public function whenInstanciateAnEntityWithIdWrongItThrowAnException()
    {
        $this->expectExceptionMessage('Argument');
        $entity = new Wallet();
        $entity->setId(234);
    }
}
