<?php

namespace App\Tests\Machine\Domain\Services;

use App\Machine\Domain\Services\CalculateReturn;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;
use PHPUnit\Framework\TestCase;

class CalculateReturnTest extends TestCase
{

    /**
     * @test
     * @dataProvider dataForReturns
     * @param int $amount
     * @param UnitCoinsGroup $input
     * @param bool $areChangeExpected
     * @param UnitCoinsGroup $coinsToReturnExpected
     */
    public function service_return_coins_properly(
        int $amount,
        UnitCoinsGroup $input,
        bool $areChangeExpected,
        UnitCoinsGroup $coinsToReturnExpected
    ) {
        $service = new CalculateReturn();
        [$areChange, $coinsToReturn] = $service->calculate($amount, $input);

        $this->assertTrue($areChange);
        $this->assertEquals($coinsToReturnExpected, $coinsToReturn);
    }

    public function dataForReturns()
    {
        return [
            [
                100,
                new UnitCoinsGroup(1,1,1,1),
                true,
                new UnitCoinsGroup(0,0,0,1)
            ],
            [
                65,
                new UnitCoinsGroup(10,7,0,0),
                true,
                new UnitCoinsGroup(1,6,0,0)
            ],
            [
                65,
                new UnitCoinsGroup(10,0,2,10),
                true,
                new UnitCoinsGroup(3,0,2,0)
            ],
            [
                335,
                new UnitCoinsGroup(10,1,0,10),
                true,
                new UnitCoinsGroup(5,1,0,3)
            ]
        ];
    }
}
