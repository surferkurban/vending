<?php

declare(strict_types=1);

namespace App\Machine\Application\DTOs;

class CoinsStatus
{
    private int $coin005;
    private int $coin010;
    private int $coin025;
    private int $coin100;
    private int $totalCoins;

    public function __construct(int $coin005, int $coin010, int $coin025, int $coin100, int $totalCoints)
    {
        $this->coin005 = $coin005;
        $this->coin010 = $coin010;
        $this->coin025 = $coin025;
        $this->coin100 = $coin100;
        $this->totalCoins = $totalCoints;
    }

    /**
     * @return int
     */
    public function getCoin005(): int
    {
        return $this->coin005;
    }

    /**
     * @param int $coin005
     * @return CoinsStatus
     */
    public function setCoin005(int $coin005): CoinsStatus
    {
        $this->coin005 = $coin005;

        return $this;
    }

    /**
     * @return int
     */
    public function getCoin010(): int
    {
        return $this->coin010;
    }

    /**
     * @param int $coin010
     * @return CoinsStatus
     */
    public function setCoin010(int $coin010): CoinsStatus
    {
        $this->coin010 = $coin010;

        return $this;
    }

    /**
     * @return int
     */
    public function getCoin025(): int
    {
        return $this->coin025;
    }

    /**
     * @param int $coin025
     * @return CoinsStatus
     */
    public function setCoin025(int $coin025): CoinsStatus
    {
        $this->coin025 = $coin025;

        return $this;
    }

    /**
     * @return int
     */
    public function getCoin100(): int
    {
        return $this->coin100;
    }

    /**
     * @param int $coin100
     * @return CoinsStatus
     */
    public function setCoin100(int $coin100): CoinsStatus
    {
        $this->coin100 = $coin100;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCoins(): int
    {
        return $this->totalCoins;
    }

    /**
     * @param int $totalCoins
     * @return CoinsStatus
     */
    public function setTotalCoins(int $totalCoins): CoinsStatus
    {
        $this->totalCoins = $totalCoins;

        return $this;
    }


}