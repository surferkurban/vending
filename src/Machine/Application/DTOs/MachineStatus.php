<?php

namespace App\Machine\Application\DTOs;

class MachineStatus
{
    private array $items;
    private CoinsStatus $wallet;
    private CoinsStatus $cashBox;

    /**
     * MachineStatus constructor.
     * @param array $items
     * @param CoinsStatus $wallet
     * @param CoinsStatus $cashBox
     */
    public function __construct(array $items, CoinsStatus $wallet, CoinsStatus $cashBox)
    {
        $this->items = $items;
        $this->wallet = $wallet;
        $this->cashBox = $cashBox;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return MachineStatus
     */
    public function setItems(array $items): MachineStatus
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return CoinsStatus
     */
    public function getWallet(): CoinsStatus
    {
        return $this->wallet;
    }

    /**
     * @param CoinsStatus $wallet
     * @return MachineStatus
     */
    public function setWallet(CoinsStatus $wallet): MachineStatus
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * @return CoinsStatus
     */
    public function getCashBox(): CoinsStatus
    {
        return $this->cashBox;
    }

    /**
     * @param CoinsStatus $cashBox
     * @return MachineStatus
     */
    public function setCashBox(CoinsStatus $cashBox): MachineStatus
    {
        $this->cashBox = $cashBox;

        return $this;
    }

}