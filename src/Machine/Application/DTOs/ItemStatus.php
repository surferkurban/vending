<?php

namespace App\Machine\Application\DTOs;

class ItemStatus
{
    private string $name;
    private int $price;
    private int $stock;

    /**
     * ItemStatus constructor.
     * @param string $name
     * @param int $price
     * @param int $stock
     */
    public function __construct(string $name, int $price, int $stock)
    {
        $this->name = $name;
        $this->price = $price;
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

}