<?php

namespace App\Machine\Application\Commands;

use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Entities\Machine;
use App\Machine\Domain\Repositories\ItemRepositoryInterface;
use App\Machine\Domain\Repositories\MachineRepositoryInterface;
use App\Machine\Domain\Services\CalculateReturn;
use App\Machine\Domain\Transforms\CashBoxToUnitCoinsGroup;
use App\Machine\Domain\Transforms\WalletToUnitCoinsGroup;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;
use Brick\Math\Exception\IntegerOverflowException;

class SelectItemHandle
{
    /** @var MachineRepositoryInterface */
    private MachineRepositoryInterface $machineRepository;
    /** @var ItemRepositoryInterface */
    private ItemRepositoryInterface $itemRepository;
    /** @var WalletToUnitCoinsGroup */
    private WalletToUnitCoinsGroup $walletToUnitCoinsGroup;
    /** @var CashBoxToUnitCoinsGroup */
    private CashBoxToUnitCoinsGroup $cashBoxToUnitCoinsGroup;
    /** @var CalculateReturn */
    private CalculateReturn $calculateChargeService;

    public function __construct(
        MachineRepositoryInterface $machineRepository,
        ItemRepositoryInterface $itemRepository,
        WalletToUnitCoinsGroup $walletToUnitCoinsGroup,
        CashBoxToUnitCoinsGroup $cashBoxToUnitCoinsGroup,
        CalculateReturn $calculateChargeService
    ) {
        $this->machineRepository = $machineRepository;
        $this->itemRepository = $itemRepository;
        $this->walletToUnitCoinsGroup = $walletToUnitCoinsGroup;
        $this->cashBoxToUnitCoinsGroup = $cashBoxToUnitCoinsGroup;
        $this->calculateChargeService = $calculateChargeService;
    }

    public function __invoke(SelectItemCommand $command)
    {
        /** @var Machine $machine */
        $machine = $this->machineRepository->findMachine($command->getMachineId());
        $item = $this->itemRepository->findByPosition(
            $command->getMachineId(),
            $command->getSelector()
        );

        return $this->serve($machine, $item);
    }

    private function serve(Machine $machine, Item $item) :SelectItemResponse
    {
        /** @var UnitCoinsGroup $totalCoins */
        $totalCoins = $this->getTotalUnitCoinsGroupInMachine($machine);

        $coinGroupInWallet = $this->walletToUnitCoinsGroup->transform($machine->getWallet());
        if ($coinGroupInWallet->getTotalAmount() < $item->getPrice()) {
            return new SelectItemResponse(400, 'Insufficient coins in Wallet.');
        }

        /** @var UnitCoinsGroup $changeUnitCoinsGroup */
        list($thereIsChange, $changeUnitCoinsGroup) =
            $this->calculateChargeService->calculate(
                $coinGroupInWallet->getTotalAmount()-$item->getPrice(),
                $totalCoins
            );

        if (!$thereIsChange ||
            $item->getStock() == 0
        ) {
            $string = "cambio: $thereIsChange , stock = ".$item->getStock() ." wallet: " .
                $coinGroupInWallet->getTotalAmount() . " <  Price: ".$item->getPrice();
            return new SelectItemResponse(400, $string);
        }

        try {
            $machine->pickItem($item, $totalCoins, $changeUnitCoinsGroup);
        } catch (IntegerOverflowException $e) {
            return new SelectItemResponse(400, 'Item not exist');
        }

        $this->machineRepository->save($machine);

        return new SelectItemResponse(200, "Served ". $thereIsChange);
    }

    private function getTotalUnitCoinsGroupInMachine(Machine $machine): UnitCoinsGroup
    {
        $coinGroup = $this->walletToUnitCoinsGroup->transform($machine->getWallet());
        $coinGroup = $this->cashBoxToUnitCoinsGroup
            ->transform($machine->getCashBox())
            ->addUnitCoins($coinGroup);
        return $coinGroup;
    }

}