<?php

declare(strict_types=1);

namespace App\Machine\Application\Commands;

use App\Machine\Domain\ValueObjects\UnitCoinsGroup;
use Ramsey\Uuid\Uuid;

class InsertCoinCommand extends UnitCoinsGroup
{
    private Uuid $machineId;

    public function __construct(Uuid $machineId, int $coin005, int $coin010, int $coin025, int $coin100)
    {
        parent::__construct($coin005, $coin010, $coin025, $coin100);
        $this->machineId = $machineId;
    }

    public function getMachineId(): Uuid
    {
        return $this->machineId;
    }
}