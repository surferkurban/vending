<?php

namespace App\Machine\Application\Commands;

class SelectItemResponse
{
    private int $status;
    private string $message;

    public function __construct(int $status, string $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getMessage(): string
    {
        return $this->message;
    }


}