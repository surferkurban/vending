<?php

declare(strict_types=1);

namespace App\Machine\Application\Commands;

use Ramsey\Uuid\Uuid;

class ReturnWalletCommand
{
    private Uuid $machineId;

    public function __construct(Uuid $machineId)
    {
        $this->machineId = $machineId;
    }

    public function getMachineId(): Uuid
    {
        return $this->machineId;
    }

}