<?php

namespace App\Machine\Application\Commands;

use Ramsey\Uuid\Uuid;

class SelectItemCommand
{
    private Uuid $machineId;
    private int $selector;

    public function __construct(Uuid $machineId, int $selector)
    {
        $this->machineId = $machineId;
        $this->selector = $selector;
    }

    public function getMachineId(): Uuid
    {
        return $this->machineId;
    }

    public function getSelector(): int
    {
        return $this->selector;
    }
}