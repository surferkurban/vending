<?php

declare(strict_types=1);

namespace App\Machine\Application\Commands;

use App\Machine\Domain\Repositories\MachineRepositoryInterface;

class ReturnWalletHandler
{
    /** @var MachineRepositoryInterface */
    private MachineRepositoryInterface $machineRepository;

    public function __construct(MachineRepositoryInterface $machineRepository)
    {
        $this->machineRepository = $machineRepository;
    }

    public function __invoke(ReturnWalletCommand $command)
    {
        $machine = $this->machineRepository->findMachine($command->getMachineId());
        $wallet = $machine->getWallet();

        $response = new ReturnWalletResponse(
            $wallet->getCoin005(),
            $wallet->getCoin010(),
            $wallet->getCoin025(),
            $wallet->getCoin100()
        );

        $wallet->setCoin005(0);
        $wallet->setCoin010(0);
        $wallet->setCoin025(0);
        $wallet->setCoin100(0);

        $this->machineRepository->save($machine);

        return $response;
    }
}