<?php

namespace App\Machine\Application\Commands;

use App\Machine\Domain\ValueObjects\UnitCoinsGroup;
use Ramsey\Uuid\Uuid;

class ChargeCommand extends UnitCoinsGroup
{
    private Uuid $machineId;
    private int $stock01;
    private int $stock02;
    private int $stock03;

    public function __construct(
        Uuid $machineId,
        int $coin005,
        int $coin010,
        int $coin025,
        int $coin100,
        int $stock01,
        int $stock02,
        int $stock03
    )
    {
        parent::__construct($coin005, $coin010, $coin025, $coin100);
        $this->machineId = $machineId;
        $this->stock01 = $stock01;
        $this->stock02 = $stock02;
        $this->stock03 = $stock03;
    }

    public function getMachineId(): Uuid
    {
        return $this->machineId;
    }

    public function getStock01(): int
    {
        return $this->stock01;
    }

    public function getStock02(): int
    {
        return $this->stock02;
    }

    public function getStock03(): int
    {
        return $this->stock03;
    }

}