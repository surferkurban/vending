<?php

namespace App\Machine\Application\Commands;

use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Repositories\MachineRepositoryInterface;

class ChargeHandle
{
    /** @var MachineRepositoryInterface */
    private MachineRepositoryInterface $machineRepository;

    public function __construct(MachineRepositoryInterface $machineRepository)
    {
        $this->machineRepository = $machineRepository;
    }

    public function __invoke(ChargeCommand $command)
    {
        $machine = $this->machineRepository->findMachine($command->getMachineId());
        $cashBox = $machine->getCashBox();
        $cashBox->setCoin005($command->getCoin005());
        $cashBox->setCoin010($command->getCoin010());
        $cashBox->setCoin025($command->getCoin025());
        $cashBox->setCoin100($command->getCoin100());
        /** @var Item $items1 */
        $items1 = $machine->getItems()->filter(function($item) {
            /** @var Item $item */
            return $item->getPosition() == 1;
        })->first();
        $items1->setStock($command->getStock01());

        $items2 = $machine->getItems()->filter(function($item) {
            /** @var Item $item */
            return $item->getPosition() == 2;
        })->first();
        $items2->setStock($command->getStock02());

        $items3 = $machine->getItems()->filter(function($item) {
            /** @var Item $item */
            return $item->getPosition() == 3;
        })->first();
        $items3->setStock($command->getStock03());

        $this->machineRepository->save($machine);
    }
}