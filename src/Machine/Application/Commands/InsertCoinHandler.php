<?php

declare(strict_types=1);

namespace App\Machine\Application\Commands;

use App\Machine\Domain\Repositories\MachineRepositoryInterface;

class InsertCoinHandler
{
    /** @var MachineRepositoryInterface */
    private MachineRepositoryInterface $machineRepository;

    public function __construct(MachineRepositoryInterface $machineRepository)
    {
        $this->machineRepository = $machineRepository;
    }

    public function __invoke(InsertCoinCommand $command)
    {
        $machine = $this->machineRepository->findMachine($command->getMachineId());
        $wallet = $machine->getWallet();
        $wallet->setCoin005($wallet->getCoin005() + $command->getCoin005());
        $wallet->setCoin010($wallet->getCoin010() + $command->getCoin010());
        $wallet->setCoin025($wallet->getCoin025() + $command->getCoin025());
        $wallet->setCoin100($wallet->getCoin100() + $command->getCoin100());
        $this->machineRepository->save($machine);
    }

}