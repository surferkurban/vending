<?php

namespace App\Machine\Application\Queries;

use Ramsey\Uuid\Uuid;

class GetStatusQuery
{
    private Uuid $machineId;

    public function __construct(Uuid $machineId)
    {
        $this->machineId = $machineId;
    }

    public function getMachineId(): Uuid
    {
        return $this->machineId;
    }
}