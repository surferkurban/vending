<?php

namespace App\Machine\Application\Queries;

use App\Machine\Application\DTOs\CoinsStatus;
use App\Machine\Application\DTOs\ItemStatus;
use App\Machine\Application\DTOs\MachineStatus;
use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Repositories\MachineRepositoryInterface;
use App\Machine\Domain\Transforms\CashBoxToUnitCoinsGroup;
use App\Machine\Domain\Transforms\WalletToUnitCoinsGroup;
use Doctrine\Common\Collections\ArrayCollection;

class GetStatusHandle
{
    /**
     * @var MachineRepositoryInterface
     */
    private MachineRepositoryInterface $machineRepository;
    /**
     * @var WalletToUnitCoinsGroup
     */
    private WalletToUnitCoinsGroup $walletToUnitCoinsGroup;
    /**
     * @var CashBoxToUnitCoinsGroup
     */
    private CashBoxToUnitCoinsGroup $cashBoxToUnitCoinsGroup;

    public function __construct(
        MachineRepositoryInterface $machineRepository,
        WalletToUnitCoinsGroup $walletToUnitCoinsGroup,
        CashBoxToUnitCoinsGroup $cashBoxToUnitCoinsGroup
    ) {
        $this->machineRepository = $machineRepository;
        $this->walletToUnitCoinsGroup = $walletToUnitCoinsGroup;
        $this->cashBoxToUnitCoinsGroup = $cashBoxToUnitCoinsGroup;
    }

    public function __invoke(GetStatusQuery $query)
    {
        $machine = $this->machineRepository->findMachine($query->getMachineId());
        $wallet = $machine->getWallet();
        $walletDTO = new CoinsStatus(
            $wallet->getCoin005(),
            $wallet->getCoin010(),
            $wallet->getCoin025(),
            $wallet->getCoin100(),
            $this->walletToUnitCoinsGroup->transform($wallet)->getTotalAmount()
        );
        $cashBox = $machine->getCashBox();
        $cashBoxDTO = new CoinsStatus(
            $cashBox->getCoin005(),
            $cashBox->getCoin010(),
            $cashBox->getCoin025(),
            $cashBox->getCoin100(),
            $this->cashBoxToUnitCoinsGroup->transform($cashBox)->getTotalAmount()
        );

        $itemList = [];
        /** @var ArrayCollection $items */
        $items = $machine->getItems();
        /** @var Item $item */
        foreach ($items as $item) {
            $itemStatus = new ItemStatus(
                $item->getName(),
                $item->getPrice(),
                $item->getStock()
            );
            $itemList[$item->getPosition()] = $itemStatus;
        }

        return new GetStatusResponse($itemList, $walletDTO, $cashBoxDTO);
    }

}