<?php

declare(strict_types=1);

namespace App\Machine\Domain\Entities;

use App\Machine\Domain\Exceptions\ItemNotExistException;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;

class Machine
{
    private Uuid $id;
    private int $status;
    private CashBox $cashBox;
    private Wallet $wallet;
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getCashBox(): ?CashBox
    {
        return $this->cashBox;
    }

    public function setCashBox(CashBox $cashBox): void
    {
        $this->cashBox = $cashBox;
    }

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function setWallet(Wallet $wallet): void
    {
        $this->wallet = $wallet;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items): void
    {
        $this->items = $items;
    }
    public function addItem(Item $item)
    {
        if (!$this->getItems()->contains($item)) {
            $this->getItems()->add($item);
            $item->setMachine($this);
        }
    }
    public function removeItem(Item $item)
    {
        $this->items->remove($item->getId());
        if ($this->getItems()->contains($item)) {
            $this->getItems()->removeElement($item);
            $item->setMachineId(null);
        }
    }

    public function pickItem(
        Item $item,
        UnitCoinsGroup $totalCoins,
        UnitCoinsGroup $changeUnitCoinsGroup
    ): void {
        if (!$this->items->contains($item)) {
            throw new ItemNotExistException("No existe el item indicado");
        }
        $item->setStock($item->getStock() - 1);

        $wallet = $this->getWallet();
        $wallet->setCoin005(0);
        $wallet->setCoin010(0);
        $wallet->setCoin025(0);
        $wallet->setCoin100(0);

        $cashBox = $this->getCashBox();
        $cashBox->setCoin005($totalCoins->getCoin005() - $changeUnitCoinsGroup->getCoin005());
        $cashBox->setCoin010($totalCoins->getCoin010() - $changeUnitCoinsGroup->getCoin010());
        $cashBox->setCoin025($totalCoins->getCoin025() - $changeUnitCoinsGroup->getCoin025());
        $cashBox->setCoin100($totalCoins->getCoin100() - $changeUnitCoinsGroup->getCoin100());
    }
}