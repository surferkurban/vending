<?php

declare(strict_types=1);

namespace App\Machine\Domain\Entities;

use Ramsey\Uuid\Uuid;

class Wallet
{
    private Uuid $id;
    private int $coin100;
    private int $coin025;
    private int $coin010;
    private int $coin005;

    public function getCoin100(): int
    {
        return $this->coin100;
    }

    public function setCoin100(int $coin100): void
    {
        $this->coin100 = $coin100;
    }

    public function getCoin025(): int
    {
        return $this->coin025;
    }

    public function setCoin025(int $coin025): void
    {
        $this->coin025 = $coin025;
    }

    public function getCoin010(): int
    {
        return $this->coin010;
    }

    public function setCoin010(int $coin010): void
    {
        $this->coin010 = $coin010;
    }

    public function getCoin005(): int
    {
        return $this->coin005;
    }

    public function setCoin005(int $coin005): void
    {
        $this->coin005 = $coin005;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): void
    {
        $this->id = $id;
    }
}