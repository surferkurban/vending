<?php

declare(strict_types=1);

namespace App\Machine\Domain\ValueObjects;

use App\Machine\Domain\Constants\CoinConstants;

class UnitCoinsGroup
{
    private int $coin005;
    private int $coin010;
    private int $coin025;
    private int $coin100;

    public function __construct(int $coin005, int $coin010, int $coin025, int $coin100)
    {
        $this->coin005 = $coin005;
        $this->coin010 = $coin010;
        $this->coin025 = $coin025;
        $this->coin100 = $coin100;
    }

    public function getCoin005(): int
    {
        return $this->coin005;
    }

    public function getCoin010(): int
    {
        return $this->coin010;
    }

    public function getCoin025(): int
    {
        return $this->coin025;
    }

    public function getCoin100(): int
    {
        return $this->coin100;
    }

    public function getTotalAmount(): int
    {
        return $this->getAmountCoin005() +
            $this->getAmountCoin010() +
            $this->getAmountCoin025() +
            $this->getAmountCoin100();
    }

    public function getAmountCoin005(): int
    {
        return $this->coin005 * CoinConstants::VALUE_COIN005;
    }

    public function getAmountCoin010(): int
    {
        return $this->coin010 * CoinConstants::VALUE_COIN010;
    }

    public function getAmountCoin025(): int
    {
        return $this->coin025 * CoinConstants::VALUE_COIN025;
    }

    public function getAmountCoin100(): int
    {
        return $this->coin100 * CoinConstants::VALUE_COIN100;
    }

    public function addUnitCoins(UnitCoinsGroup $unitCoinsGroup) : self
    {
        return new self(
            $this->getCoin005() + $unitCoinsGroup->getCoin005(),
            $this->getCoin010() + $unitCoinsGroup->getCoin010(),
            $this->getCoin025() + $unitCoinsGroup->getCoin025(),
            $this->getCoin100() + $unitCoinsGroup->getCoin100()
        );
    }
}