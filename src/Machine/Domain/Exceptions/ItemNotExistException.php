<?php

declare(strict_types=1);

namespace App\Machine\Domain\Exceptions;

class ItemNotExistException extends \Exception
{

}