<?php

declare(strict_types=1);

namespace App\Machine\Domain\Transforms;

use App\Machine\Domain\Entities\Wallet;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;

class WalletToUnitCoinsGroup
{
    public function transform(Wallet $wallet): UnitCoinsGroup
    {
        return new UnitCoinsGroup(
            $wallet->getCoin005(),
            $wallet->getCoin010(),
            $wallet->getCoin025(),
            $wallet->getCoin100()
        );
    }
}