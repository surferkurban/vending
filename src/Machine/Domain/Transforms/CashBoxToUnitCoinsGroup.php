<?php

declare(strict_types=1);

namespace App\Machine\Domain\Transforms;

use App\Machine\Domain\Entities\CashBox;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;

class CashBoxToUnitCoinsGroup
{
    public function transform(CashBox $cashBox): UnitCoinsGroup
    {
        return new UnitCoinsGroup(
            $cashBox->getCoin005(),
            $cashBox->getCoin010(),
            $cashBox->getCoin025(),
            $cashBox->getCoin100()
        );
    }
}