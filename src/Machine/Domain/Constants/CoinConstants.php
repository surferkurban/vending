<?php

declare(strict_types=1);

namespace App\Machine\Domain\Constants;

class CoinConstants
{
    const COIN_BASE = 100;
    const VALUE_COIN005 = 5;
    const VALUE_COIN010 = 10;
    const VALUE_COIN025 = 25;
    const VALUE_COIN100 = 100;
}