<?php

declare(strict_types=1);

namespace App\Machine\Domain\Services;

use App\Machine\Domain\Constants\CoinConstants;
use App\Machine\Domain\ValueObjects\UnitCoinsGroup;

class CalculateReturn
{
    /**
     * @return array [bool {if the machine has coins to return}, UnitCoinsGroup {coins to return}]
     */
    public function calculate(int $returnAmount, UnitCoinsGroup $unitCoinsGroup): array
    {
        $returnAmountBase = $returnAmount;
        //Coins 100
        $c100 = $this->calculateCoins100($unitCoinsGroup, $returnAmountBase);
        //Coins 025
        $c025 = $this->calculateCoins025($unitCoinsGroup, $returnAmountBase);
        //Coins 010
        $c010 = $this->calculateCoins010($unitCoinsGroup, $returnAmountBase);
        //Coins 005
        $c005 = $this->calculateCoins005($unitCoinsGroup, $returnAmountBase);
        $charge = new UnitCoinsGroup($c005, $c010, $c025, $c100);

        return [$returnAmountBase == 0, $charge];
    }

    private function calculateCoins100(UnitCoinsGroup $unitCoinsGroup, int &$returnAmountBase): int
    {
        return $this->calculateCoinsOfValue(
            CoinConstants::VALUE_COIN100,
            $unitCoinsGroup->getCoin100(),
            $returnAmountBase
        );
    }

    private function calculateCoins025(UnitCoinsGroup $unitCoinsGroup, int &$returnAmountBase): int
    {
        return $this->calculateCoinsOfValue(
            CoinConstants::VALUE_COIN025,
            $unitCoinsGroup->getCoin025(),
            $returnAmountBase
        );
    }

    private function calculateCoins010(UnitCoinsGroup $unitCoinsGroup, int &$returnAmountBase): int
    {
        return $this->calculateCoinsOfValue(
            CoinConstants::VALUE_COIN010,
            $unitCoinsGroup->getCoin010(),
            $returnAmountBase
        );
    }

    private function calculateCoins005(UnitCoinsGroup $unitCoinsGroup, int &$returnAmountBase): int
    {
        return $this->calculateCoinsOfValue(
            CoinConstants::VALUE_COIN005,
            $unitCoinsGroup->getCoin005(),
            $returnAmountBase
        );
    }
    private function calculateCoinsOfValue(int $coinValue, int $maxNumberOfCoins, int &$returnAmountBase): int
    {
        $coins = intdiv($returnAmountBase, $coinValue);
        if ($coins > $maxNumberOfCoins) {
            $coins = $maxNumberOfCoins;
        }
        $returnAmountBase -= $coins * $coinValue;

        return $coins;
    }
}