<?php

namespace App\Machine\Domain\Repositories;

use App\Machine\Domain\Entities\Machine;
use Ramsey\Uuid\Uuid;

interface MachineRepositoryInterface
{
    public function findMachine(Uuid $uuid): ?Machine;
    public function save(Machine $machine);
}