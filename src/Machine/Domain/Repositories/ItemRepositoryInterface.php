<?php

namespace App\Machine\Domain\Repositories;

use App\Machine\Domain\Entities\Item;
use Ramsey\Uuid\Uuid;

interface ItemRepositoryInterface
{
    public function findByPosition(Uuid $machineId, int $position): ?Item;
    public function findAllByMachine(Uuid $machineId): ?array;
}