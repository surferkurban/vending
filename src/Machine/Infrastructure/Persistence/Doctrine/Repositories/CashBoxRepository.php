<?php

declare(strict_types=1);

namespace App\Machine\Infrastructure\Persistence\Doctrine\Repositories;

use App\Machine\Domain\Entities\CashBox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CashBox|null find($id, $lockMode = null, $lockVersion = null)
 * @method CashBox|null findOneBy(array $criteria, array $orderBy = null)
 * @method CashBox[]    findAll()
 * @method CashBox[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CashBoxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CashBox::class);
    }

    // /**
    //  * @return Machine[] Returns an array of Machine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Machine
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
