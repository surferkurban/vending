<?php

declare(strict_types=1);

namespace App\Machine\Infrastructure\Persistence\Doctrine\Repositories;

use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Repositories\ItemRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository implements ItemRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findByPosition(Uuid $machineId, int $position): ?Item
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.position = :val')
            ->setParameter('val', $position)
            ->getQuery()
            ->getOneOrNullResult()
            ;

    }

    public function findAllByMachine(Uuid $machineId): ?array
    {
        return $this->createQueryBuilder('m')
            //->andWhere('m.exampleField = :val')
            //->setParameter('val', $value)
            ->orderBy('m.position', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Machine[] Returns an array of Machine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Machine
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
