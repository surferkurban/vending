<?php

namespace App\Machine\Infrastructure\Ui\Http\Controllers;

use App\Machine\Application\Queries\GetStatusQuery;
use App\Machine\Application\Queries\GetStatusResponse;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class WebController extends AbstractController
{
    public function index()
    {
        $query = new GetStatusQuery(
            Uuid::fromString($this->getParameter('machine.id'))
        );
        $envelope = $this->dispatchMessage($query);

        /** @var GetStatusResponse $getStatusResponse */
        $getStatusResponse = $envelope->last(HandledStamp::class)->getResult();

        return new Response($this->renderView("index.html.twig",['machine' => $getStatusResponse]));
    }

}