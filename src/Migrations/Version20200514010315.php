<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200514010315 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wallet (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', coin100 INT NOT NULL, coin025 INT NOT NULL, coin010 INT NOT NULL, coin005 INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE machines (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', cash_box_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', wallet_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', status INT NOT NULL, UNIQUE INDEX UNIQ_F1CE8DEDEAFC2947 (cash_box_id), UNIQUE INDEX UNIQ_F1CE8DED712520F3 (wallet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cashbox (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', coin100 INT NOT NULL, coin025 INT NOT NULL, coin010 INT NOT NULL, coin005 INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE items (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', machine_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', position INT NOT NULL, price INT NOT NULL, name VARCHAR(255) NOT NULL, stock INT NOT NULL, INDEX IDX_E11EE94DF6B75B26 (machine_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE machines ADD CONSTRAINT FK_F1CE8DEDEAFC2947 FOREIGN KEY (cash_box_id) REFERENCES cashbox (id)');
        $this->addSql('ALTER TABLE machines ADD CONSTRAINT FK_F1CE8DED712520F3 FOREIGN KEY (wallet_id) REFERENCES wallet (id)');
        $this->addSql('ALTER TABLE items ADD CONSTRAINT FK_E11EE94DF6B75B26 FOREIGN KEY (machine_id) REFERENCES machines (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE machines DROP FOREIGN KEY FK_F1CE8DED712520F3');
        $this->addSql('ALTER TABLE items DROP FOREIGN KEY FK_E11EE94DF6B75B26');
        $this->addSql('ALTER TABLE machines DROP FOREIGN KEY FK_F1CE8DEDEAFC2947');
        $this->addSql('DROP TABLE wallet');
        $this->addSql('DROP TABLE machines');
        $this->addSql('DROP TABLE cashbox');
        $this->addSql('DROP TABLE items');
    }
}
