<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Machine\Domain\Entities\CashBox;
use App\Machine\Domain\Entities\Item;
use App\Machine\Domain\Entities\Machine;
use App\Machine\Domain\Entities\Wallet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class AppFixtures extends Fixture
{

    private string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function load(ObjectManager $manager)
    {
        /*$walletId =  Uuid::uuid4();
        $cashBoxId = Uuid::uuid4();
        echo "El uuid de marras es :" . $this->id . "\n";
        $machineId = Uuid::fromString($this->id);
        echo "El uuid de marras es :" . $machineId->toString() . "\n";
        $wallet = new Wallet();
        $wallet->setCoin005(0);
        $wallet->setCoin010(0);
        $wallet->setCoin025(0);
        $wallet->setCoin100(0);
        $wallet->setId($walletId);


        $cashBox = new CashBox();
        $cashBox->setCoin005(10);
        $cashBox->setCoin010(10);
        $cashBox->setCoin025(10);
        $cashBox->setCoin100(10);
        $cashBox->setId($cashBoxId);

        $machine = new Machine();
        $machine->setCashBox($cashBox);
        $machine->setWallet($wallet);
        $machine->setStatus(0);
        $machine->setId($machineId);

        $manager->persist($wallet);
        $manager->persist($cashBox);
        $manager->persist($machine);
        $manager->flush();

        $item1 = new Item();
        $item1->setName('Water');
        $item1->setPosition(1);
        $item1->setPrice(65);
        $manager->persist($item1);

        $item2 = new Item();
        $item2->setName('Juice');
        $item2->setPosition(2);
        $item2->setPrice(100);
        $manager->persist($item2);

        $item3 = new Item();
        $item3->setName('Soda');
        $item3->setPosition(3);
        $item3->setPrice(150);
        $manager->persist($item3);

        $manager->flush();*/


        //$walletId =  Uuid::uuid4();
        //$cashBoxId = Uuid::uuid4();
        $machineId = Uuid::fromString($this->id);

        $wallet = new Wallet();
        $wallet->setCoin005(0);
        $wallet->setCoin010(0);
        $wallet->setCoin025(0);
        $wallet->setCoin100(0);
        //$wallet->setId($walletId);


        $cashBox = new CashBox();
        $cashBox->setCoin005(10);
        $cashBox->setCoin010(10);
        $cashBox->setCoin025(10);
        $cashBox->setCoin100(10);
        //$cashBox->setId($cashBoxId);

        $item1 = new Item();
        $item1->setName('Water');
        $item1->setPosition(1);
        $item1->setPrice(65);
        $item1->setStock(20);


        $item2 = new Item();
        $item2->setName('Juice');
        $item2->setPosition(2);
        $item2->setPrice(100);
        $item2->setStock(20);


        $item3 = new Item();
        $item3->setName('Soda');
        $item3->setPosition(3);
        $item3->setPrice(150);
        $item3->setStock(20);

        $machine = new Machine();
        $machine->setId($machineId);
        $machine->setCashBox($cashBox);
        $machine->setWallet($wallet);
        $machine->setStatus(0);


        $manager->persist($machine);

        $machine->addItem($item1);
        $machine->addItem($item2);
        $machine->addItem($item3);
        $manager->persist($machine);

        $manager->flush();


    }
}
